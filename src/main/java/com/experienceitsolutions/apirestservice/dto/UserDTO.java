package com.experienceitsolutions.apirestservice.dto;

import jakarta.validation.constraints.NotBlank;

public class UserDTO {
    @NotBlank(message = "The username is obligatory.")
    private String username;

    @NotBlank(message = "The firstname is obligatory.")
    private String firstname;

    @NotBlank(message = "The lastname is obligatory.")
    private String lastname;

    public UserDTO() {

    }

    public UserDTO(String username, String firstname, String lastname) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

}
