package com.experienceitsolutions.apirestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApirestserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApirestserviceApplication.class, args);
	}

}
