package com.experienceitsolutions.apirestservice.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.experienceitsolutions.apirestservice.dto.UserDTO;
import com.experienceitsolutions.apirestservice.model.User;
import com.experienceitsolutions.apirestservice.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public ResponseEntity<List<User>> findAll() {
        return new ResponseEntity<List<User>>(userRepository.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<?> findById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            Map<String, List<String>> errors = Collections.singletonMap("errors",
                    Collections.singletonList("The user ID doesn't exist."));
            return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(user.get(), HttpStatus.OK);
    }

    public ResponseEntity<?> save(UserDTO userDTO) {
        Optional<User> user = userRepository.findByUsername(userDTO.getUsername());
        if (!user.isEmpty()) {
            Map<String, List<String>> errors = Collections.singletonMap("errors",
                    Collections.singletonList("The username already exists."));
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }

        User newUser = new User(userDTO.getUsername(), userDTO.getFirstname(), userDTO.getLastname());
        userRepository.save(newUser);
        return new ResponseEntity<User>(newUser, HttpStatus.OK);
    }

    public ResponseEntity<?> update(UserDTO userDTO, Long id) {
        if (!existsById(id)) {
            Map<String, List<String>> errors = Collections.singletonMap("errors",
                    Collections.singletonList("The user ID doesn't exists."));
            return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
        }

        if (!userRepository.findByUsername(userDTO.getUsername()).isEmpty()) {
            Map<String, List<String>> errors = Collections.singletonMap("errors",
                    Collections.singletonList("The username already exists."));
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }

        User user = userRepository.findById(id).get();
        user.setUsername(userDTO.getUsername());
        user.setFirstname(userDTO.getFirstname());
        user.setLastname(userDTO.getLastname());
        userRepository.save(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);

    }

    public ResponseEntity<?> deleteById(Long id) {
        if (!existsById(id)) {
            Map<String, List<String>> errors = Collections.singletonMap("errors",
                    Collections.singletonList("The user ID doesn't exists."));
            return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
        }

        userRepository.deleteById(id);
        return new ResponseEntity<User>(HttpStatus.OK);
    }

    public boolean existsById(Long id) {
        return userRepository.existsById(id);
    }

}
