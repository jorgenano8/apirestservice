package com.experienceitsolutions.apirestservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.experienceitsolutions.apirestservice.model.User;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

}
