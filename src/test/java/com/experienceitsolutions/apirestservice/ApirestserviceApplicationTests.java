package com.experienceitsolutions.apirestservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.experienceitsolutions.apirestservice.controller.UserController;
import com.experienceitsolutions.apirestservice.dto.UserDTO;
import com.experienceitsolutions.apirestservice.model.User;
import com.experienceitsolutions.apirestservice.repository.UserRepository;
import com.experienceitsolutions.apirestservice.service.UserService;

@SpringBootTest
class ApirestserviceApplicationTests {

	@MockBean
	UserRepository userRepository;

	@Autowired
	UserController userController;

	@Autowired
	UserService userService;

	// GET METHODS

	@Test
	public void getAllUsersOK() throws Exception {
		List<User> users = new ArrayList<User>();
		users.add(createUser());
		Mockito.when(userRepository.findAll()).thenReturn(users);
		ResponseEntity<?> responseEntity = userController.findAll();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		Mockito.verify(userRepository).findAll();
	}

	@Test
	public void getUserById_Found() throws Exception {
		User user = createUser();
		Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
		ResponseEntity<?> responseEntity = userController.findById(user.getId());

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		Mockito.verify(userRepository).findById(user.getId());
	}

	@Test
	public void getUserById_NotFound() throws Exception {
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		ResponseEntity<?> responseEntity = userController.findById(Mockito.anyLong());

		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
	}

	// POST METHODS

	@Test
	public void postUserOK() throws Exception {
		UserDTO userDTO = createUserDTO();
		Mockito.when(userRepository.findByUsername(userDTO.getUsername())).thenReturn(Optional.empty());
		ResponseEntity<?> responseEntity = userController.save(userDTO);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		Mockito.verify(userRepository).save(Mockito.any());
	}

	@Test
	public void postUserKO_UsernameAlreadyExists() throws Exception {
		User user = createUser();
		UserDTO userDTO = createUserDTO();
		Mockito.when(userRepository.findByUsername(userDTO.getUsername())).thenReturn(Optional.of(user));
		ResponseEntity<?> responseEntity = userController.save(userDTO);

		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		Mockito.verify(userRepository, Mockito.never()).save(Mockito.any());
	}

	// PUT METHODS

	@Test
	public void putUserOK() throws Exception {
		User user = createUser();
		UserDTO userDTO = createUserDTO();
		Mockito.when(userService.existsById(Mockito.anyLong())).thenReturn(true);
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		ResponseEntity<?> responseEntity = userController.update(userDTO, Mockito.anyLong());

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		Mockito.verify(userRepository).save(user);
	}

	@Test
	public void putUserKO_UserNotFound() throws Exception {
		UserDTO userDTO = createUserDTO();
		Mockito.when(userService.existsById(Mockito.anyLong())).thenReturn(false);
		ResponseEntity<?> responseEntity = userController.update(userDTO, Mockito.anyLong());

		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		Mockito.verify(userRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	public void putUserKO_UsernameAlreadyExists() throws Exception {
		User user = createUser();
		UserDTO userDTO = createUserDTO();
		Mockito.when(userService.existsById(Mockito.anyLong())).thenReturn(true);
		Mockito.when(userRepository.findByUsername(userDTO.getUsername())).thenReturn(Optional.of(user));
		ResponseEntity<?> responseEntity = userController.update(userDTO, Mockito.anyLong());

		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		Mockito.verify(userRepository, Mockito.never()).save(Mockito.any());
	}

	// DELETE METHODS

	@Test
	public void deleteUserById_Found() throws Exception {
		User user = createUser();
		Mockito.when(userService.existsById(user.getId())).thenReturn(true);
		ResponseEntity<?> responseEntity = userController.deleteById(user.getId());

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		Mockito.verify(userRepository).deleteById(user.getId());
	}

	@Test
	public void deleteUserById_NotFound() throws Exception {
		Mockito.when(userService.existsById(Mockito.anyLong())).thenReturn(false);
		ResponseEntity<?> responseEntity = userController.deleteById(Mockito.anyLong());

		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		Mockito.verify(userRepository, Mockito.never()).deleteById(Mockito.anyLong());
	}

	private User createUser() {
		User user = new User();
		user.setUsername("jhondoe");
		user.setFirstname("Jhon");
		user.setLastname("Doe");
		user.setId(1L);
		return user;
	}

	private UserDTO createUserDTO() {
		UserDTO user = new UserDTO();
		user.setUsername("jhondoe");
		user.setFirstname("Jhon");
		user.setLastname("Doe");
		return user;
	}

}
